package Lista_1;

public class NumeroMenorMaior {

	public static void main(String[] args) {

		NumeroMenorMaior menor = new NumeroMenorMaior();
		
		menor.menor(3);
		System.out.println();
		menor.menor(5);
		System.out.println();
		menor.menor(8);
		
	}

	public void menor(int K){
		
		System.out.print(K);
		
		if (K == 0)
			K = -1;
		else if (K > 1)
			menor(K-1);
		
		System.out.print(K);
					
	}
}
