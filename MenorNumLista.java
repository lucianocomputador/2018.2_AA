package Lista_1;

import java.util.ArrayList;

public class MenorNumLista {

	public static void main(String[] args) {

		MenorNumLista mnum = new MenorNumLista();

		ArrayList<Integer> nums = new ArrayList<>();

		nums.add(10);
		nums.add(4);
		nums.add(6);
		nums.add(4);
		nums.add(3);
		nums.add(2);
		nums.add(8);
		nums.add(9);
		nums.add(2);
		nums.add(6);

		System.out.println( mnum.menor(nums) );
		
	}

	public int menor(ArrayList<Integer> K) {

		if (K.size()==1)
			return K.get(0);
		else{
			int aux = K.get(0);
			K.remove(0);
			return Math.min(aux, menor(K));
		}
		
	}
}
