package Lista_1;

public class NumeroMenor {

	public static void main(String[] args) {

		NumeroMenor menor = new NumeroMenor();
		
		menor.menor(3);
		
		menor.menor(2);
		
		menor.menor(0);
		
	}

	public void menor(int K){
		
		if (K == 0)
			K = -1;
		else if (K > 1)
			menor(K-1);
		
		System.out.println(K);
					
	}
}
