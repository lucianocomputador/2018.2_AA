package Lista_1;

public class QtDigitos {

	public static void main(String[] args) {
		QtDigitos qtdD = new QtDigitos();

		System.out.println(qtdD.qtd(9));
		
		System.out.println(qtdD.qtd(154));

	}

	public int qtd(int K) {
		if (K > 9) {
			return 1 + qtd(K / 10);
		}
		return 1;

	}
	
	
}
