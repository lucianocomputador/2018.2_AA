package Lista_1;

public class TorreHanoi {

	public static void main(String[] args) {
		
		TorreHanoi th = new TorreHanoi();
		
		th.hanoi(3, "A", "C", "B");
		
	}
	
	public void hanoi(int discos, String origem, String destino, String aux){
		
		if (discos == 1)
			mover(origem, destino);
		else {
			discos--;
            hanoi(discos, origem, aux, destino);
            mover(origem, destino);
            hanoi(discos, aux, destino, origem);
         
		}
	}

	public void mover(String origem, String destino) {
        System.out.println(origem + " >>> " + destino);
    }
}
