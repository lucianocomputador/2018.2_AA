package Lista_1;

public class KMensagem {

	public static void main(String[] args) {

		KMensagem msg = new KMensagem();

		msg.kMensagem("Olá Mundo!", 3);

		msg.kMensagem("Testando...", 1);

		msg.kMensagem("Adeus", 0);

	}

	public void kMensagem(String msg, int K) {

		if (K >= 1) {
			kMensagem(msg, K - 1);
			System.out.println(msg);
		}

	}

}
