package Lista_1;

public class Fatorial {
	
	public static void main(String[] args) {
		
		Fatorial fat = new Fatorial();
		
		System.out.println(fat.fatorial(3));
		
		System.out.println(fat.fatorial(6));
		
		System.out.println(fat.fatorial(0));
	}

	public int fatorial(int K){
		
		if (K<2)
			return 1;
		else
			return K * fatorial(K-1);
		
	}
	
}
