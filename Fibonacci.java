package Lista_1;

public class Fibonacci {

	public static void main(String[] args) {
		
		Fibonacci fib = new Fibonacci();
		
		System.out.println(fib.fb(3));
		
		System.out.println(fib.fb(6));
		
		System.out.println(fib.fb(0));

	}
	
	public int fb(int K){
		
		if (K < 2)
			return K;

		return fb(K - 1) + fb(K - 2);
	}

}
