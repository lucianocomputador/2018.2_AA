package Lista_1;

public class CalcProduto {

	public static void main(String[] args) {

		CalcProduto prod = new CalcProduto();

		System.out.println(prod.produto(4, 6));
		
		System.out.println(prod.produto(1, 11));
		
		System.out.println(prod.produto(0, 6));
	}

	public int produto(int x, int K) {

		if (K == 0)
			return 0 ;
		else if (K >= 1) 
			return produto (x, K-1) + x;	
		
		return 0;
		
	}
}
